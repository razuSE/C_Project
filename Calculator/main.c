#include <stdio.h>
#include <stdlib.h>
#include<conio.h>
#include<windows.h>

void add();
void sub();
void mp();
void division();
void about();

int main()
{
    int opt;
    system("COLOR B9");

    printf("\t\t\t[Welcome To SimpleCalc]\n\n");
    printf("\nChoose an operation : \n");
    printf("\n [1] Add\n [2] Subtract\n [3] Multiply\n [4] Divide\n [5] About\n");

    printf("\nYour Option : ");
    scanf("%d", &opt);

    switch(opt){
        case 1: add();
        break;
        case 2: sub();
        break;
        case 3: mp();
        break;
        case 4: division();
        break;
        case 5: about();
        break;
        default: printf("\nNot Valid\n");
        break;

    }

    return 0;

}

void add()
{
    system("cls");
    printf("\t\t[ADDITION]\n\n");
    float i,input,total = 0;
    char ch;
    printf("\n|Note|\n Give an input,\n Then Press Enter,\n Then Press '+' for next input,\n Then press enter,\n Press '=' to get result.\n" );
    for ( ; ; ){
        printf ("\nInput : ");
        scanf (" %f", &input);
        total = total + input;

        ch = getch ();
        if (ch == 43){
            continue;
        }
        else if(ch == 61){
            break;
        }
    }
    printf ("\nAddition Result : %.2f\n\n", total);
}

void sub()
{
    system("cls");
    printf("\t\t[SUBTRACTION]\n\n");
    float input1, input2, result;
    printf("\n//Enter two numbers to subtract\n");
    printf("\nFirst Input : ");
    scanf("%f", &input1);
    printf("\nSecond Input : ");
    scanf("%f", &input2);
    result = input1 - input2;
    printf("\nSubtraction Result : %0.2f\n\n", result);
}

void mp()
{
    system ("cls");
    printf("\t\t[MULTIPLICATION]\n\n");
    float input,total = 1;
    char ch;
    printf("\n|Note|\n Give an input,\n Then Press Enter,\n Then Press '*' for next input,\n Then press enter,\n Press '=' to get result.\n" );
    for (; ;){
        printf("\nInput : ");
        scanf(" %f", &input);
        total = total * input;
        ch = getch();
        if(ch == 42){
                continue;
        }
        if(ch == 61){
                break;
        }
    }
    printf ("\nMultiplication Result : %0.2f\n\n", total);
}

void division()
{
    system("cls");
    printf("\t\t[DIVISION]\n\n");
    float input3, input4, result;
    printf("\nEnter the Dividend : ");
    scanf("%f", &input3);
    printf("\nEnter the Divisor : ");
    scanf("%f", &input4);
    result = (input3/input4);
    printf("\nSubtraction Result : %0.2f\n\n", result);
}

void about()
{
    system("cls");
    printf("\nDeveloped By : Raju Ahmed Rony\n");
    printf("\nVersion : 1.0\n");
}
