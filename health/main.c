#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>

void mainMenu();
void matDetect();
void bmiCalc();
void hStatus();
void ss();
void addRecord();
void recPlus();
void readRecord();
void hTips();
void hospitals();
void about();

int main()
{
    system("COLOR 74");
    int i;
    for(; ;){
    printf("\n\t\t\t[Health Care System (v 1.0)] \n");

    system("cls");
    printf("\n\n//NOTE: Enter 0 to quit or any key to continue// \n");

    printf("\nYour option : ");
    scanf("%d", &i);

    system("cls");
    if(i == 0){
        break;
    }
    else{
        printf("\t\t\t [Main Menu] \n\n");
        mainMenu();
        printf("\nThank You! Press any key to go to main menu... \n");
    }

//    getch();
    }
//    getch();

    return 0;
}

void mainMenu()
{
    int m;

    printf("[1] Maturity Detector\n[2] BMI Calculator\n[3] Health Status Tracker\n[4] Record/View Status\n[5] Health Tips\n[6] Hospitals Tracker\n[7] About");

    printf("\n\nChoose an option : ");
    scanf("%d", &m);

    if(m == 1){
        matDetect();
    }
    else if(m == 2){
        bmiCalc();
    }
    else if(m == 3){
        hStatus();
    }
    else if(m == 4){
        ss();
    }
    else if(m == 5){
        hTips();
    }
    else if(m == 6){
        hospitals();
    }
    else if(m == 7){
        about();
    }
    else{
        printf("\nIncorrect Keyword\n");
    }
    return;
}

void matDetect()
{
    system("cls");
    printf("\t\t\t\t[Maturity Detector]\n");
    int age;

    printf("\nEnter your age : ");
    scanf(" %d", &age);

    if(age < 18)
    {
        printf("\nYou are Premature! \n");
    }

    else
    {
        printf("\nYou are Mature! \n");
    }
    getch();
    return;
}

void bmiCalc()
{
    system("cls");
    printf("\t\t\t\t[BMI Calculator]\n");
    int height, weight, age2;
    float BMI;

    printf("\nNote: This option is for Matures only, Enter your age : ");
    scanf("%d", &age2);

    if(age2 < 18){
        system("cls");
        printf("\nSorry you are Premature and system is redirected to Main Menu... \n\n");
        mainMenu();
        getch();
    }
    else{
    printf("\nEnter your height in inch : ");
    scanf("%d", &height);

    printf("\nEnter your weight in kg : ");
    scanf("%d", &weight);

    BMI = weight/((height*0.025)*(height*0.025));

    printf("\nBMI = %0.2f \n", BMI);

    if(BMI < 18.5){
        printf("\nStatus : Underweight \n", BMI);
    }

    else if(BMI >= 18.5 && BMI <= 25){
        printf("\nStatus : Normal weight \n", BMI);
    }

    else if(BMI > 25 && BMI <= 30){
        printf("\nStatus : Overweight \n", BMI);
    }
    else{
        printf("\nStatus : Obese \n", BMI);
    }
    }
    getch();
    return;
}

void hStatus()
{
    system("cls");
    printf("\t\t\t\t[Health Status Tracker]\n");
    int sym;
    printf("\n//Choose a symptom: \n");
    printf("\n[1] Excessive sweating, chills, headache, body & muscle aches, weakness, fatigue, cough, rash, sinus congestion. \n");
    printf("\n[2] Sore throat, nausea, vomiting, headache, sinus pressure, runny nose, night sweats and postnasal drip. \n");

    printf("\nYour Option : ");
    scanf("%d", &sym);

    system("cls");
    FILE *fp;
    if(sym == 1){
        printf("\nStatus : You have Fever! \n\n");
        fp = fopen("fever.txt", "r");
    }
    else if(sym == 2){

        printf("\nStatus : You have Cough & Cold! \n\n");
        fp = fopen("cc.txt", "r");
    }
    else{
        printf("\nWrong Keyword! \n\n");
    }
    char singleLine[150];
    while(!feof(fp)){
        fgets(singleLine, 150, fp);
        puts(singleLine);
    }
    fclose(fp);
    getch();
    return;
}

void ss()
{
    system("cls");
    int rec;
    printf("\n\n//Options : \n\t  [1] Create New Record \n\t  [2] Add data to existing record \n\t  [3] View Record ");
    printf("\nYour Choice : ");
    scanf("%d", &rec);

    if(rec == 1){
        int chck;
        int c;
        printf("\nCreating new record will erase all your existing records. \n ");
        printf("\n\t   Type [1] to continue, or, \n\t   Type [2] to add data to existing record. \n");
        printf("\nYour Choice : ");
        scanf("%d", &chck);
        if(chck == 1){
            addRecord();
            for(; ;){
            printf("\nDo you want to add more?\n \t[1] Yes\n \t[2] No\n");
            printf("\nYour Option : ");
            scanf("%d", &c);

            if(c == 1){
                recPlus();
            }
            else if(c == 2){
                printf("\nYou are redirecting to Main Menu...\n");
                system("cls");
                mainMenu();
                getch();
            }
            else{
                printf("\nWrong Keyword !");
                getch();
            }
            }
        }
        else if(chck == 2){
            recPlus();
        }
    }
    else if(rec == 2){
        recPlus();
    }
    else if(rec == 3){
        readRecord();
    }
    getch();
    return;
}

void addRecord()
{
    system("cls");
    printf("\t\t\t\t[Add Record]\n");

    int date[15];
    int age;
    char sex[10], health[15];
    float bmi;

    FILE *fp;
    fp = fopen("record.txt", "w");

    printf("\nEnter date : ");
    scanf("%s", &date);

    printf("\nEnter age : ");
    scanf("%d", &age);

    printf("\nEnter sex : ");
    scanf("%s", &sex);

    printf("\nEnter BMI report : ");
    scanf("%f", &bmi);

    printf("\nEnter health report : ");
    scanf("%s", &health);

    fprintf(fp, "\nDate : %s", date);
    fprintf(fp, " | Age : %d", age);
    fprintf(fp, " | Sex : %s", sex);
    fprintf(fp, " | BMI : %0.2f", bmi);
    fprintf(fp, " | Health report : %s", health);

    printf("\nData recorded successfully! \n");

    fclose(fp);
    getch();
    return;
}

void recPlus()
{
    system("cls");
    printf("\t\t\t\t[Add Record to Existing Data]\n");
    int date[15];
    int age;
    char sex[10], health[15];
    float bmi;

    FILE *fp;
    fp = fopen("record.txt", "a");

    printf("\nEnter date : ");
    scanf("%s", &date);

    printf("\nEnter age : ");
    scanf("%d", &age);

    printf("\nEnter sex : ");
    scanf("%s", &sex);

    printf("\nEnter BMI report : ");
    scanf("%f", &bmi);

    printf("\nEnter health report : ");
    scanf("%s", &health);

    fprintf(fp, "\nDate : %s", date);
    fprintf(fp, " | Age : %d", age);
    fprintf(fp, " | Sex : %s", sex);
    fprintf(fp, " | BMI : %0.2f", bmi);
    fprintf(fp, " | Health report : %s", health);

    printf("\nData added to record successfully! \n");

    fclose(fp);
    getch();
    return;
}

void readRecord()
{
    system("cls");
    printf("\t\t\t\t[Record Viewer]\n");

    FILE *fp;
    fp = fopen("record.txt", "r");
    char singleLine[150];
    while(!feof(fp)){
        fgets(singleLine, 150, fp);
        puts(singleLine);
    }
    fclose(fp);
    getch();
    return;
}

void hTips()
{
    system("cls");
    int m;
    printf("\t\t\t\t[Health Tips]\n");
    printf("\n[1] Vitamin-rich foods\t");

    printf("\n\nYour option : ");
    scanf("%d", &m);

    FILE *fp;
    if(m == 1){
        fp = fopen("vitamin.txt", "r");
    }

    char singleLine[150];
    while(!feof(fp)){
        fgets(singleLine, 150, fp);
        puts(singleLine);
    }
    fclose(fp);

    getch();
    return;
}

void hospitals()
{
    system("cls");
    int d;
    printf("\t\t\t\t[Hospitals Tracker]\n");
    printf("\nPlease choose your division : \n");
    printf("\n\t[1] Dhaka\n \t[2] Barisal\n \t[3] Chittagong\n \t[4] Khulna\n \t[5] Rajshahi\n \t[6] Sylhet\n \t[7] Rangpur\n");
    printf("\nChoose an option : ");
    scanf("%d", &d);

    system("cls");
    FILE *fp;
    if(d == 1){
        printf("\n\t\t\t[Top hospitals in Dhaka]\n\n");
        fp = fopen("hdhaka.txt", "r");
    }
    else if(d == 2){
        printf("\n\t\t\t[Top hospitals in Barisal]\n\n");
        fp = fopen("hbarisal.txt", "r");
    }
    else if(d == 3){
        printf("\n\t\t\t[Top hospitals in Chittagong]\n\n");
        fp = fopen("hchittagong.txt", "r");
    }
    else if(d == 4){
        printf("\n\t\t\t[Top hospitals in Khulna]\n\n");
        fp = fopen("hkhulna.txt", "r");
    }
    else if(d == 5){
        printf("\n\t\t\t[Top hospitals in Rajshahi]\n\n");
        fp = fopen("hrajshahi.txt", "r");
    }
    else if(d == 6){
        printf("\n\t\t\t[Top hospitals in Sylhet]\n\n");
        fp = fopen("hsylhet.txt", "r");
    }
    else if(d == 7){
        printf("\n\t\t\t[Top hospitals in Rangpur]\n\n");
        fp = fopen("hrangpur.txt", "r");
    }
    else{
        printf("\nWrong Keyword !\n");
    }

    char singleLine[200];
    while(!feof(fp)){
        fgets(singleLine, 200, fp);
        puts(singleLine);
    }
    fclose(fp);
    getch();

    getch();
    return;
}

void about()
{
    system("cls");
    printf("\t\t\t\t[About]\n");

    FILE *fp;
    fp = fopen("about.txt", "r");
    char singleLine[150];
    while(!feof(fp)){
        fgets(singleLine, 150, fp);
        puts(singleLine);
    }
    fclose(fp);
    getch();
    return;
}
